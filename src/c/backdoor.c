/* ==================================================================== */
/* This file is released into the public domain */
/* ==================================================================== */
#include "backdoor.h"
#include <stdio.h>
#include <string.h>

#ifdef  __linux__
#include <pthread.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/socket.h>
#endif
/* ==================================================================== */

#define TCP_PORT 27020
#define UPD_PORT 27020
#define BIND_IP "127.0.0.1"
#define BUFF_SIZE 8092
#define RESULT_VAR "bd_result"
#define DONE_STR "done\n"
#define COMMAND_EXECSTR  "Err_Job = execstr(TMP_EXEC_STRING,\"errcatch\",\"n\");quit;"
#define COMMAND_CLEAR "clear TMP_EXEC_STRING;clear Err_Job;quit;"

void SendScilabJobBD(char *job)
{
  SciErr sciErr;
  int lencommand = 0;
  const char *command = NULL;
  command = strdup(job);
  lencommand = (int)strlen(command);
  scirun_(COMMAND_CLEAR, (long int)strlen(COMMAND_CLEAR));
  sciErr = createNamedMatrixOfString(pvApiCtx, "TMP_EXEC_STRING", 1, 1, &command);
  scirun_(COMMAND_EXECSTR, (long int)strlen(COMMAND_EXECSTR));
}

#ifdef __linux__
void *takeRequests(void* arg)
#else
DWORD WINAPI takeRequests( LPVOID arg) 
#endif
{
  	char buff[BUFF_SIZE];
  	char result[BUFF_SIZE];
	char cmd[BUFF_SIZE];
	char tmp[BUFF_SIZE];
  	char *eol,*c;
	struct sockaddr_in addr,client;
  	int socksize;
  	int m=1,n=1;
	int commandComplete;
#ifdef __linux__
	ssize_t received=0;
 	int socket = *((int*)arg);
  free(arg);
#else
	int received=0;
 	SOCKET socket = *((SOCKET*)arg);
  free(arg);
#endif
	//sprintf(cmd,"mprintf(\"\tBackDoor: new connection accepted\n\");");
	//SendScilabJobBD(cmd);
	commandComplete=0;
	strcpy(cmd,""); // Clear the command
	do  { // Accept commands through the backdoor
		received=recv(socket,buff,BUFF_SIZE,0);		
		if (received<0) {
#ifndef __linux__
			sprintf(cmd,"mprintf(\"\tBackDoor: received err %d on sock=%d\n\");",WSAGetLastError(),socket);
#else
			sprintf(cmd,"mprintf(\"\tBackDoor: received err %d on sock=%d\n\");",errno,socket);
#endif
			SendScilabJobBD(cmd);
			return NULL;
		}

		buff[received]='\0'; // Terminate string with NULL char
		//sprintf(tmp,"mprintf(\"\tBackDoor: received <%s>\n\");",buff);
		//SendScilabJobBD(tmp);
		c=buff;
		while ((eol = strchr(c,'\n'))!=NULL) { // A end-of-line char has been received
			eol[0]='\0'; // Terminate the string
			if (eol[-1]=='\r') 
				eol[-1]='\0'; // Remove \r too
			strcat(cmd,c);
			if (cmd[0]=='@') { // If the first char is the '@' we need to send the result (ans var)
				readNamedMatrixOfDouble(pvApiCtx, RESULT_VAR,&m,&n,(double*)result);
				m=send(socket,result,sizeof(double),0);
			} else if (cmd[0]=='?') { // If the first char is the '@' we need to send the result (ans var)
        double d;
				readNamedMatrixOfDouble(pvApiCtx, RESULT_VAR,&m,&n,(double*)&d);
        sprintf(tmp,"%g\n",d);
				m=send(socket,tmp,strlen(tmp),0);
			} else if (cmd[0]=='!') { // The '!' denotes that the client needs a confirmation that the job is ready
				if (cmd[1]=='\\')
			            sprintf(result,"%s",cmd+2);
			        else
			  	    sprintf(result,"%s=%s",RESULT_VAR,cmd+1);
				SendScilabJobBD(result);
				send(socket,DONE_STR,strlen(DONE_STR),0);
			} else if (cmd[0]=='\\') { // The '!' denotes that the client needs a confirmation that the job is ready
				sprintf(result,"%s",cmd+1);
				SendScilabJobBD(result);
			} else {
				sprintf(result,"%s=%s",RESULT_VAR,cmd);// run the job
				SendScilabJobBD(result);
			}
		        strcpy(cmd,""); // Blank for new command
			c=eol+1;
		}
		strcat(cmd,c); // Copy the last part for the new command
	} while (received);
}

#ifdef __linux__
void *waitTCPRequests();
#else
DWORD WINAPI waitTCPRequests( LPVOID lpParam ) ;
SOCKET listenTCP;
#endif

#ifdef  __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>

pthread_t threadTCP, threadUDP;
int listenTCP;


int openBackDoor()
{
	pthread_create( &threadTCP, NULL, waitTCPRequests, (void*) NULL);
}

int closeBackDoor()
{
	pthread_kill(threadTCP,SIGKILL);
}


#else

// Put Windows includes
//
HANDLE  hThread; 
DWORD   dummy;
int openBackDoor()
{

	hThread = CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            waitTCPRequests,       // thread function name
            NULL,	            // argument to thread function 
            0,                      // use default creation flags 
            &dummy);
	return 0;
}

int closeBackDoor()
{
	TerminateThread(hThread,dummy);
	return 0;
}
#endif

#ifdef __linux__
void *waitTCPRequests() 
#else
DWORD WINAPI waitTCPRequests( LPVOID lpParam ) 
#endif
{
//	
//
	char buff[BUFF_SIZE];
	char cmd[BUFF_SIZE];
	char result[BUFF_SIZE];
	
	char *eol,*c;
	int socksize;
	int m=1,n=1;

	struct sockaddr_in addr,client;
#ifdef __linux__
	int sock;
  	int actual_port = TCP_PORT + getuid() % 10000;
	ssize_t received=0;
	sleep(1); // Wait a little bit to scilab to be ready 
#else
  	int actual_port = TCP_PORT;
	SOCKET sock;
        SOCKET *is;
	Sleep(1000); // Wait a little bit to scilab to be ready 
#endif

	listenTCP=socket(AF_INET,SOCK_STREAM,0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(actual_port) ;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind (listenTCP, (struct sockaddr *) &addr, sizeof (addr))<0) {
		sprintf(cmd,"mprintf(\"\tBackDoor: TCP port %d is in use\");",actual_port);
		SendScilabJobBD(cmd);
		return NULL;
	}
	if (listen(listenTCP,5)==0) {
		sprintf(cmd,"mprintf(\"\tBackDoor: listening to connections on TCP port %d\n\");",actual_port);
		SendScilabJobBD(cmd);
    		do {
	 	  sock=accept(listenTCP,NULL, NULL);
		  /*sprintf(cmd,"mprintf(\"\tBackDoor: new connection on TCP port %d. sock=%d err=%d\");",actual_port,sock,WSAGetLastError());
		  SendScilabJobBD(cmd);*/
#ifdef __linux__
		  pthread_t t;
      int * is = (int*)malloc(sizeof(int));
      is[0]=sock;
      pthread_create( &t, NULL, takeRequests, (void*) is);
#else 
      is = (SOCKET*)malloc(sizeof(SOCKET));
      is[0]=sock;
		  CreateThread( NULL, 0, takeRequests , (void*)is,	0, NULL);
#endif
                } while (sock>0);
	}

#ifdef __linux__
	close(listenTCP);
#else
	closesocket(listenTCP);
#endif
}
/* ==================================================================== */
